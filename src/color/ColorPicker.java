package color;

import java.awt.Dimension;
import javax.swing.JFrame;

public class ColorPicker extends JFrame{

	private static final long serialVersionUID = -285031946127995308L;

	private static ColorPicker instance = null;
	
	private RGBPanel rgb = null;
	
	public static void main(String[] args){
		ColorPicker.getInstance();
	}
	
	public static ColorPicker getInstance(){
		if(instance == null){
			instance = new ColorPicker();
		}
		return instance;
	}
	
	private ColorPicker(){
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Color Picker");
		this.setPreferredSize(new Dimension(450, 400));

		init();
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	private void init(){
		this.getContentPane().removeAll();
		
		if(rgb == null){
			rgb = new RGBPanel();
		}
		
		this.getContentPane().add(rgb);
		
		this.revalidate();
		this.repaint();
	}
}

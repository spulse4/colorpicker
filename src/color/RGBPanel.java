package color;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class RGBPanel extends JPanel{

	private static final long serialVersionUID = 2651744434779356301L;

	private int red;
	private int green;
	private int blue;
	private boolean movingRed;
	private boolean movingOther;
	private List<Integer> reds;
	private List<Integer> greens;
	private List<Integer> blues;
	
	private JTextField hex;
	private JTextField rVal;
	private JTextField gVal;
	private JTextField bVal;
	
	public RGBPanel(){
		this.setBackground(Color.LIGHT_GRAY);
		red = 0;
		green = 0;
		blue = 0;
		movingRed = false;
		movingOther = false;
		reds = new ArrayList<Integer>();
		greens = new ArrayList<Integer>();
		blues = new ArrayList<Integer>();
		for(int i = 0; i < 9; ++i){
			reds.add(0);
			greens.add(0);
			blues.add(0);
		}
		
		initText();
		init();
		initListeners();
	}
	
	private void initText(){
		rVal = new JTextField(Integer.toString(red));
		gVal = new JTextField(Integer.toString(green));
		bVal = new JTextField(Integer.toString(blue));
		rVal.setColumns(3);
		gVal.setColumns(3);
		bVal.setColumns(3);
		rVal.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {
				movingRed = false;
				movingOther = false;
				rVal.selectAll();
			}

			@Override
			public void focusLost(FocusEvent arg0) {}
			
		});
		gVal.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {
				movingRed = false;
				movingOther = false;
				gVal.selectAll();
			}

			@Override
			public void focusLost(FocusEvent arg0) {}
			
		});
		bVal.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {
				movingRed = false;
				movingOther = false;
				bVal.selectAll();
			}

			@Override
			public void focusLost(FocusEvent arg0) {}
			
		});
		
		rVal.addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent arg0) {}

			@Override
			public void keyReleased(KeyEvent arg0) {
				if(movingRed){
					int diff = 1;
					if(arg0.isShiftDown()){
						diff = 5;
					}
					if(arg0.isAltDown()){
						diff = 20;
					}
					if(arg0.isControlDown()){
						diff = 50;
					}
					if(arg0.getKeyCode() == KeyEvent.VK_UP){
						red -= diff;
						if(red < 0){
							red = 0;
						}
						rVal.setText(Integer.toString(red));
						valuesChanged();
						return;
					}
					else if(arg0.getKeyCode() == KeyEvent.VK_DOWN){
						red += diff;
						if(red > 255){
							red = 255;
						}
						rVal.setText(Integer.toString(red));
						valuesChanged();
						return;
					}
				}
				else if(movingOther){
					int diff = 1;
					if(arg0.isShiftDown()){
						diff = 5;
					}
					if(arg0.isAltDown()){
						diff = 20;
					}
					if(arg0.isControlDown()){
						diff = 50;
					}
					if(arg0.getKeyCode() == KeyEvent.VK_LEFT){
						green -= diff;
						if(green < 0){
							green = 0;
						}
						gVal.setText(Integer.toString(green));
						valuesChanged();
						return;
					}
					else if(arg0.getKeyCode() == KeyEvent.VK_RIGHT){
						green += diff;
						if(green > 255){
							green = 255;
						}
						gVal.setText(Integer.toString(green));
						valuesChanged();
						return;
					}
					else if(arg0.getKeyCode() == KeyEvent.VK_UP){
						blue -= diff;
						if(blue < 0){
							blue = 0;
						}
						bVal.setText(Integer.toString(blue));
						valuesChanged();
						return;
					}
					else if(arg0.getKeyCode() == KeyEvent.VK_DOWN){
						blue += diff;
						if(blue > 255){
							blue = 255;
						}
						bVal.setText(Integer.toString(blue));
						valuesChanged();
						return;
					}
				}
				if(arg0.getKeyCode() == KeyEvent.VK_ENTER || arg0.getKeyCode() == KeyEvent.VK_DOWN){
					gVal.requestFocus();
				}
				else if(arg0.getKeyCode() == KeyEvent.VK_UP){
					bVal.requestFocus();
				}
				else{
					try{
						red = Integer.valueOf(rVal.getText());
					}
					catch(Exception e){
						red = -1;
					}
					if(red < 0){
						red = 0;
						rVal.setText(Integer.toString(red));
						rVal.selectAll();
					}
					else if(red > 255){
						red = 255;
						rVal.setText(Integer.toString(red));
						rVal.selectAll();
					}
					valuesChanged();
				}
			}

			@Override
			public void keyTyped(KeyEvent arg0) {}
			
		});
		gVal.addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent arg0) {}

			@Override
			public void keyReleased(KeyEvent arg0) {
				if(arg0.getKeyCode() == KeyEvent.VK_ENTER || arg0.getKeyCode() == KeyEvent.VK_DOWN){
					bVal.requestFocus();
				}
				else if(arg0.getKeyCode() == KeyEvent.VK_UP){
					rVal.requestFocus();
				}
				else{
					try{
						green = Integer.valueOf(gVal.getText());
					}
					catch(Exception e){
						green = -1;
					}
					if(green < 0){
						green = 0;
						gVal.setText(Integer.toString(green));
						gVal.selectAll();
					}
					else if(green > 255){
						green = 255;
						gVal.setText(Integer.toString(green));
						gVal.selectAll();
					}
					valuesChanged();
				}
			}

			@Override
			public void keyTyped(KeyEvent arg0) {}
			
		});
		bVal.addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent arg0) {}

			@Override
			public void keyReleased(KeyEvent arg0) {
				if(arg0.getKeyCode() == KeyEvent.VK_ENTER || arg0.getKeyCode() == KeyEvent.VK_DOWN){
					rVal.requestFocus();
				}
				else if(arg0.getKeyCode() == KeyEvent.VK_UP){
					gVal.requestFocus();
				}
				else{
					try{
						blue = Integer.valueOf(bVal.getText());
					}
					catch(Exception e){
						blue = -1;
					}
					if(blue < 0){
						blue = 0;
						bVal.setText(Integer.toString(blue));
						bVal.selectAll();
					}
					else if(blue > 255){
						blue = 255;
						bVal.setText(Integer.toString(blue));
						bVal.selectAll();
					}
					valuesChanged();
				}		String hex1 = Integer.toString(Integer.valueOf(rVal.getText()), 16);
				String hex2 = Integer.toString(Integer.valueOf(gVal.getText()), 16);
				String hex3 = Integer.toString(Integer.valueOf(bVal.getText()), 16);
				
				if(hex1.length() == 1){
					hex1 = "0" + hex1;
				}
				if(hex2.length() == 1){
					hex2 = "0" + hex2;
				}
				if(hex3.length() == 1){
					hex3 = "0" + hex3;
				}

			}

			@Override
			public void keyTyped(KeyEvent arg0) {}
			
		});
	}
	
	private void init(){
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		Insets label = new Insets(10, 0, 10, 0);
		Insets value = new Insets(10, 10, 10, 20);
						
		JLabel r = new JLabel("Red:");
		JLabel g = new JLabel("Green:");
		JLabel b = new JLabel("Blue:");
						
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = label;
		c.weightx = 1.0;
		this.add(r, c);
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 1;
		c.insets = value;
		c.weightx = 0.0;
		this.add(rVal, c);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 0;
		c.gridy = 1;
		c.insets = label;
		c.weightx = 1.0;
		this.add(g, c);
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 1;
		c.insets = value;
		c.weightx = 0.0;
		this.add(gVal, c);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 0;
		c.gridy = 2;
		c.insets = label;
		c.weightx = 1.0;
		this.add(b, c);
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 1;
		c.insets = value;
		c.weightx = 0.0;
		this.add(bVal, c);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 0;
		c.gridy = 3;
		c.insets = label;
		c.weightx = 1.0;
		this.add(new JLabel("Hex: "), c);
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 1;
		c.insets = value;
		c.weightx = 0.0;
		
		String hex1 = Integer.toString(Integer.valueOf(rVal.getText()), 16);
		String hex2 = Integer.toString(Integer.valueOf(gVal.getText()), 16);
		String hex3 = Integer.toString(Integer.valueOf(bVal.getText()), 16);
		
		if(hex1.length() == 1){
			hex1 = "0" + hex1;
		}
		if(hex2.length() == 1){
			hex2 = "0" + hex2;
		}
		if(hex3.length() == 1){
			hex3 = "0" + hex3;
		}
		hex = new JTextField("000000");
		hex.setColumns(5);
		this.add(hex, c);
		
		this.revalidate();
		this.repaint();
	}
	
	private void initListeners(){
		this.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent arg0) {}

			@Override
			public void mouseEntered(MouseEvent arg0) {}

			@Override
			public void mouseExited(MouseEvent arg0) {}

			@Override
			public void mousePressed(MouseEvent arg0) {
				int x = arg0.getX();
				int y = arg0.getY();
				if(x > 29 && x < 286 && y > 30 && y < 286){
					movingOther = true;
					movingRed = false;
					green = x - 30;
					blue = y - 30;
					gVal.setText(Integer.toString(green));
					bVal.setText(Integer.toString(blue));
					valuesChanged();
				}
				else if(x > 290 && x < 315 && y > 29 && y < 286){
					movingRed = true;
					movingOther = false;
					red = y - 30;
					rVal.setText(Integer.toString(red));
					valuesChanged();
				}
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				int x = arg0.getX();
				int y = arg0.getY();
				if(y > 300 && y < 325){
					if(x % 30 > 25){
						return;
					}
					int column = (x / 30) - 1;
					if(column < 0 || column > 8){
						return;
					}
					
					reds.set(column, red);
					greens.set(column, green);
					blues.set(column, blue);
					
					valuesChanged();
				}
			}
			
		});
		
		this.addMouseMotionListener(new MouseMotionListener(){

			@Override
			public void mouseDragged(MouseEvent arg0) {
				if(!movingRed && !movingOther){
					return;
				}
				int x = arg0.getX();
				int y = arg0.getY();
				if(movingRed){
					if(y < 30){
						red = 0;
					}
					else if(y > 285){
						red = 255;
					}
					else{
						red = y - 30;
					}
					rVal.setText(Integer.toString(red));
					valuesChanged();
				}
				else{
					if(x < 30){
						green = 0;
					}
					else if(x > 285){
						green = 255;
					}
					else{
						green = x - 30;
					}
					
					if(y < 30){
						blue = 0;
					}
					else if(y > 285){
						blue = 255;
					}
					else{
						blue = y - 30;
					}
					gVal.setText(Integer.toString(green));
					bVal.setText(Integer.toString(blue));
					valuesChanged();
				}
			}

			@Override
			public void mouseMoved(MouseEvent arg0) {}
			
		});
	}
	
	public void valuesChanged(){
		String hex1 = Integer.toString(Integer.valueOf(rVal.getText()), 16);
		String hex2 = Integer.toString(Integer.valueOf(gVal.getText()), 16);
		String hex3 = Integer.toString(Integer.valueOf(bVal.getText()), 16);
		
		if(hex1.length() == 1){
			hex1 = "0" + hex1;
		}
		if(hex2.length() == 1){
			hex2 = "0" + hex2;
		}
		if(hex3.length() == 1){
			hex3 = "0" + hex3;
		}
		hex.setText(hex1 + hex2 + hex3);
		this.repaint();
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		Graphics2D g2 = (Graphics2D)g;
		
		g2.setColor(Color.BLACK);
		g2.drawRect(29, 29, 257, 257);
		
		g2.setColor(Color.BLUE);
		for(int i = 0; i < 256; ++i){
			for(int j = 0; j < 256; ++j){
				g2.setColor(new Color(red, i, j));
				g2.drawLine(30 + i, 30 + j, 30 + i, 30 + j);
			}
		}
		
		g2.setColor(Color.RED);
		g2.fillRect(300, 30, 5, 256);
		
		g2.setColor(Color.BLACK);
		g2.drawString("Red", 291, 20);
		g2.drawString("Green", 150, 20);
		g2.drawString("Blue", 2, 160);
		g2.fillOval(295, 23 + red, 14, 14);
				
		g2.setColor(Color.WHITE);
		g2.drawOval(26 + green, 26 + blue, 7, 7);

		//Current Box
		g2.setColor(Color.BLACK);
		g2.drawRect(350, 30, 50, 50);
		g2.setColor(new Color(red, green, blue));
		g2.fillRect(351, 31, 49, 49);
		
		for(int i = 0; i < 9; ++i){
			g2.setColor(Color.BLACK);
			g2.drawRect((i + 1) * 30, 300, 25, 25);
			g2.setColor(new Color(reds.get(i), greens.get(i), blues.get(i)));
			g2.fillRect(((i + 1) * 30) + 1, 301, 24, 24);
			g2.setColor(Color.BLACK);
			g2.drawString(Integer.toString(reds.get(i)), (i + 1) * 30, 336);
			g2.drawString(Integer.toString(greens.get(i)), (i + 1) * 30, 346);
			g2.drawString(Integer.toString(blues.get(i)), (i + 1) * 30, 356);
		}
	}
}
